@extends('layouts.layout')

@section('content')
    <div class="row row-cols-lg-4 row-cols-md-4 row-cols-sm-2">
     @foreach($photos as $photo)
             <div class="card p-2 m-1 ">
                 <img src="{{asset('/storage/' . $photo->image)}}" class="card-img-top" alt="...">
                 <div class="card-body row">
                     <div class="col">
                         <p class="card-text">{{$photo->name}}</p>
                         <p class="card-footer">{{$photo->user->name}}</p>
                         <p>{{$photo->created_at}}</p>
                     </div>
                 </div>
             </div>
        @endforeach
    </div>
@endsection
